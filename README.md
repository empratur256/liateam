##liateam test
نیازمندی های این پروژه
```
docker compose
php7.3
mysql
composer
```
اگر با داکر پروژه را اجرا می کنید همان داکر کافیست
ابتدا با استفاده از دستور زیر در ترمینال پروژه را دریافت کنید

```
git clone https://gitlab.com/empratur256/liateam.git
```
حال اگر بر روی سیستم داکر نصب دارید دستور زیر را وارد کنید
```
docker-compose up -d
```

حال با زدن دستور زیر پکیج های آن را نصب کنید
```
composer install
```
برای داکر
```
docker-compose exec liateam composer install
```
حال اطلاعات دیتابیس خود را در .env وارد کنید
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
حال برای ساهت دیتابیس دستور زیر را وارد کنید
```
php arrisan migrate
```
برای داکر
```
docker-compose exec liateam php arrisan migrate
```
حال جهت ساخته شدن کابر ادمین دستور زیر را وارد کنید
```
php arrisan db:seed
```
برای داکر
```
docker-compose exec liateam php arrisan db:seed
```
برای تنظیم توکن برای احراز هویت دستور زیر را وارد کنید
```
php arrisan passport:install
```
برای داکر
```
docker-compose exec liateam php arrisan passport:install
```
حال برای استفاده فایل
```
lia team.postman_collection.json
```
را در پست من خود ایمورت کنید و برای ادامه از داکومنت پست من استفاده نمایید