<?php

namespace App\Managers;


use App\Models\Permission;

class PermissionsManager
{
    public function getAll()
    {
        return Permission::get();
    }
}