<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 11:55 AM
 */

namespace App\Managers;


use App\Contracts\ManagerContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseManager implements ManagerContract
{
    protected $model;

    public function add($data)
    {
        $this->model->fill($data)->save();
        return $this->getModel();
    }

    public function getModel():Model
    {
        return $this->model;
    }

    /**
     * @param $model
     * @return Model
     */
    public function setModel(Model $model):Model
    {
        return $this->model = $model;
    }

    public function find($id)
    {
        return $this->setModel($this->model->newQuery()->find($id));
    }

    public function update($data)
    {
        return $this->model->update($data);
    }

    public function delete($id)
    {
        return $this->model->newQuery()->whereId($id)->delete();
    }

    /**
     * @param int $limit
     * @param array $where
     * @return Collection
     */
    public function get($limit = 20, $where = [])
    {
        $query = $this->model->newQuery()->where($where);
        if($limit){
            return $query->paginate($limit);
        }
        return $query->get();
    }

    abstract public function canDelete();
}