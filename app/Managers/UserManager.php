<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/13/18
 * Time: 2:43 PM
 */

namespace App\Managers;


use App\Models\User;
use App\Models\UserGroup;

class UserManager extends BaseManager
{
    public function __construct()
    {
        $this->setModel(new User());
    }

    public static function getRoles($id)
    {
        return User::find($id)->roles()->get();
    }

    public function canDelete()
    {
        return true;
    }

    public function updateRoles($rolesIds)
    {
        $this->getModel()->syncRoles($rolesIds);
    }

    public function get($limit = 20, $where = [])
    {
        $query = User::query();
        return $query->paginate($limit);
    }
}