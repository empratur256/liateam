<?php

namespace App\Providers;

use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Dusterio\LumenPassport\LumenPassport::routes($this->app);
//        \Dusterio\LumenPassport\LumenPassport::routes($this->app, ['prefix' => 'v1/oauth']);
//        LumenPassport::allowMultipleTokens();
//        LumenPassport::tokensExpireIn(Carbon::now()->addYears(50), 2);
    }
}
