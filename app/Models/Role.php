<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as SpatieRole;


class Role extends SpatieRole
{
    use HasFactory;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
