<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 12:25 PM
 */

namespace App\Contracts;


use Illuminate\Database\Eloquent\Model;

interface ManagerContract
{
    public function add($data);
    public function find($id);
    public function update($data);
    public function getModel():Model;
    public function setModel(Model $model);
    public function delete($id);
    public function get($limit,$where = []);
    public function canDelete();
}