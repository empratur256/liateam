<?php

namespace App\Http\Requests\Users;

use Urameshibr\Requests\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string|max:255',
            'mobile'   => 'required|digits:11|unique:users,mobile',
            'email'    => 'required|string|max:255|unique:users,email',
            'password' => 'required|string|min:8',
        ];
    }
}
