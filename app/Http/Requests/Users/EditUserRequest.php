<?php

namespace App\Http\Requests\Users;


use Urameshibr\Requests\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int)$this->route()[2]['id'];
        $data = [
            'name'   => 'required|string|max:255',
            'mobile' => 'required|digits:11|unique:users,mobile,' . $id,
            'email'  => 'required|string|max:255|unique:users,email,' . $id,
        ];
        if ($this->input('password',null)) {
            return ['password' => 'min:8|max:255'];
        }

        return $data;
    }
}
