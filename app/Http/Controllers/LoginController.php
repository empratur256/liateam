<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(LoginRequest $request)
    {
        $user = User::whereEmail($request->email)->first();
        $token = null;
        if (Hash::check($request->password ,$user->password) ) {
            $token = $user->createToken(config('app.name'))->accessToken;
            return ['access_token' => $token];
        }
        return response()->json([trans('ایمیل یا پسورد اشتباه است')], 422);
    }

}
